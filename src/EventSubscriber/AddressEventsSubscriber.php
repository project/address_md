<?php

namespace Drupal\address_md\EventSubscriber;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Replaces format and subdivisions info for MD addresses.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class AddressEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPath;

  /**
   * Cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a AddressEventsSubscriber object.
   *
   * @param Drupal\Core\Extension\ExtensionPathResolver $extension_path
   *   Extension path resolver.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   */
  public function __construct(ExtensionPathResolver $extension_path, CacheBackendInterface $cache) {
    $this->extensionPath = $extension_path;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      AddressEvents::ADDRESS_FORMAT => ['onAddressFormat'],
      AddressEvents::SUBDIVISIONS => ['onSubdivisions'],
    ];
  }

  /**
   * Alters the address format for Republic of Moldova.
   *
   * @param \Drupal\address\Event\AddressFormatEvent $event
   *   The address format event.
   */
  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();

    if (isset($definition['country_code']) && $definition['country_code'] == 'MD') {
      $definition['format'] = "%givenName %familyName\n%organization\n%administrativeArea-%locality\n%dependentLocality\n%postalCode\n%addressLine1\n%addressLine2";
      $definition['subdivision_depth'] = 2;
      $definition['administrative_area_type'] = AdministrativeAreaType::DISTRICT;
      $event->setDefinition($definition);
    }
  }

  /**
   * Replaces default subdivisions list for Republic of Moldova.
   *
   * @param \Drupal\address\Event\SubdivisionsEvent $event
   *   The subdivisions event.
   */
  public function onSubdivisions(SubdivisionsEvent $event) {
    $parents = $event->getParents();
    if (empty($parents)) {
      return;
    }

    $countryCode = array_shift($parents);
    $group = strtoupper($countryCode);
    if ($parents) {
      // @see CommerceGuys\Addressing\Subdivision\SubdivisionRepository::buildGroup().
      $group .= str_repeat('-', count($parents));
      $group .= hash('tiger128,3', implode('-', $parents));
    }

    if ($countryCode == 'MD') {
      $definitions = $event->getDefinitions();

      $cache_key = 'address.subdivisions.' . $group;
      $filename = $this->extensionPath
        ->getPath('module', 'address_md') . '/json/' . $group . '.json';

      if ($cached = $this->cache->get($cache_key)) {
        $definitions = $cached->data;
      }
      elseif ($raw_definition = @file_get_contents($filename)) {
        $definitions = json_decode($raw_definition, TRUE);
        $this->cache->set($cache_key, $definitions, CacheBackendInterface::CACHE_PERMANENT, ['subdivisions']);
      }

      $event->setDefinitions($definitions);
    }
  }

}
